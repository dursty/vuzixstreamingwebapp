const express = require('express')
const fs = require('fs')
const path = require('path')
const app = express()
const util = require('util')
const bodyParser = require('body-parser')

app.use(express.static(path.join(__dirname, 'public')))
app.use(bodyParser.json());

app.listen(3000, function () {
    console.log('Listening on port 3000!')
})



app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/index.html'))
    console.log("test");
})

app.post('/route1', function(req, res) {
  console.log("Recieved");
    var post = req.body;
    console.log(util.inspect(post, false, null, true /* enable colors */))
    console.log(post.name);
    res.send("Sending some message");
});

app.get('/route1', function(req, res) {
  console.log("recieved video get");
});
app.get('/video', function(req, res) {
  const path = 'assets/sample.mp4'
  const stat = fs.statSync(path)
  const fileSize = stat.size
  const range = req.headers.range

  if (range) {
    const parts = range.replace(/bytes=/, "").split("-")
    const start = parseInt(parts[0], 10)
    const end = parts[1]
      ? parseInt(parts[1], 10)
      : fileSize-1

    const chunksize = (end-start)+1
    const file = fs.createReadStream(path, {start, end})
    const head = {
      'Content-Range': `bytes ${start}-${end}/${fileSize}`,
      'Accept-Ranges': 'bytes',
      'Content-Length': chunksize,
      'Content-Type': 'video/mp4',
    }

    res.writeHead(206, head)
    file.pipe(res)
  } else {
    const head = {
      'Content-Length': fileSize,
      'Content-Type': 'video/mp4',
    }
    res.writeHead(200, head)
    fs.createReadStream(path).pipe(res)
  }
})

